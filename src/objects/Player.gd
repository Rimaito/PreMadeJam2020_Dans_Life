extends KinematicBody2D
class_name Player

signal hit_floor(player);
signal died;

onready var spawnpoint :Vector2 = $".".position;
onready var camera_smooth_speed :float = $Focus/PlayerCamera.smoothing_speed;

var velocity := Vector2.ZERO;
var dashed := false;
var jumped := true;

var disable_input := false;

var was_in_air := false;
var died := false;

var walking_speed := 50.0;
const DASH_BOOST := 120;
const DASH_BOOST_DISTRIBUTION := Vector2(1.5,1.05);
const ACCELLERATION := 12.0;
const DEACCELLERATION := 20.0;
const GRAVITY := 3;
const JUMP_STRENGTH := 100.0;

func _ready() -> void:
	GlobalVariables.player = self;

func _physics_process(delta: float) -> void:
	died = false;
	
	if (is_on_floor()):
		velocity.y = 0;
		jumped = false;
		dashed = false;
		if (was_in_air && $EnemyDetector.get_overlapping_bodies().size() == 0):
			was_in_air = false;
			emit_signal("hit_floor");
	else:
		was_in_air = true;
	velocity.y += GRAVITY;
	
#walking logic
	var walk_direction := Vector2.ZERO;
	
	if (!disable_input):
		if (Input.is_action_pressed("move_right")):
			walk_direction.x += Input.get_action_strength("move_right");
		if (Input.is_action_pressed("move_left")):
			walk_direction.x -= Input.get_action_strength("move_left");

	var accelleration := DEACCELLERATION;
	if (walk_direction.dot(velocity)):
		accelleration = ACCELLERATION;

	if (!is_on_floor()):
		accelleration *= 0.4;

	velocity.x = velocity.linear_interpolate(walk_direction * walking_speed, delta * accelleration).x;


	#Jumping logic
	if (!disable_input):
		if (Input.is_action_pressed("jump") && is_on_floor() && !jumped):
			velocity.y -= JUMP_STRENGTH;
			jumped = true;
			#Particles
			var dust_particles = preload("res://src/particles/Dust.tscn").instance()
			dust_particles.position = self.position;
			$".".get_parent().add_child(dust_particles);
		if (Input.is_action_just_released("jump") && !is_on_floor() && velocity.y < 0 && jumped):
			velocity.y *= 0.5;

	#dashing logic
	if (Input.is_action_just_pressed("jump") && !is_on_floor() && !dashed):
		if (Input.is_action_pressed("move_down")):
			walk_direction.y += Input.get_action_strength("move_down");
		if (Input.is_action_pressed("move_up")):
			walk_direction.y -= Input.get_action_strength("move_up");
	
		if (walk_direction != Vector2.ZERO):
			velocity = walk_direction * DASH_BOOST_DISTRIBUTION * DASH_BOOST;
			dashed = true;
			#particles
			$DashParticles.direction = walk_direction * -1;
			$DashParticles.restart();
	
	update_animation();
	velocity.y = move_and_slide(velocity, Vector2(0,-1), true).y;
	

func update_animation() -> void:
	if (velocity.x > 0):
		$AnimatedSprite.flip_h = false;
	else:
		$AnimatedSprite.flip_h = true;

	if (Input.is_action_pressed("move_right") && is_on_floor() && $AnimatedSprite.animation != "walk"):
		$AnimatedSprite.play("walk");
	elif (Input.is_action_pressed("move_left") && is_on_floor() && $AnimatedSprite.animation != "walk"):
		$AnimatedSprite.play("walk");
	elif (!is_on_floor() && !dashed):
		$AnimatedSprite.play("jump");
	elif (!is_on_floor() && dashed):
		$AnimatedSprite.play("dash");
	if (is_on_floor() && round(velocity.x) == 0):
		$AnimatedSprite.play("idle");

func collided(body: Node) -> void:
	if (body.is_in_group("enemy") && !died):
		die();
		died = true;
	elif (body.is_in_group("items")):
		collect_item(body);
		

func die() -> void:
	var death_particles = preload("res://src/particles/Death.tscn").instance();
	death_particles.position = self.position;
	get_parent().add_child(death_particles);
	
	self.position = spawnpoint;
	$Focus/PlayerCamera.smoothing_speed = 1;
	$CameraSpeedResetTimer.start(0.2);
	emit_signal("died");

func collect_item(item: Node) -> void:
	item.collected = true;
	
	match item.ITEM_TYPE:
		GlobalVariables.ItemTypes.GEM:
			dashed = false;
			item.collected = true;
			connect("hit_floor", item, "reset", [self]);
			
		GlobalVariables.ItemTypes.DANDELION:
			connect("hit_floor", item, "final_collected");
			connect("died", item, "reset", [self]);
		
		GlobalVariables.ItemTypes.SPAWNPOINT:
			spawnpoint = item.position + item.get_parent().position;
		
		GlobalVariables.ItemTypes.SOUL:
			item.show_tray();

func _on_CameraSpeedResetTimer_timeout() -> void:
	$Focus/PlayerCamera.smoothing_speed *= 1.5;
	if ($Focus/PlayerCamera.smoothing_speed < camera_smooth_speed):
		$CameraSpeedResetTimer.start(0.2);
	else:
		$Focus/PlayerCamera.smoothing_speed = camera_smooth_speed;

func set_camera(left: int, bottom: int, right: int, top: int) -> void:
	$Camera2D.limit_left = left;
	$Camera2D.limit_bottom = bottom;
	$Camera2D.limit_right = right;
	$Camera2D.limit_top = top;
