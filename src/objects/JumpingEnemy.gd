extends KinematicBody2D

export(int) var jump_height := 140;

var velocity := Vector2();
var posX :float;

var jumping = false;

func _ready() -> void:
	posX = position.x
	pass

func _physics_process(delta: float) -> void:
	position.x = posX;
	if (GlobalVariables.player != null):
		if (GlobalVariables.player.position.x > position.x):
			$AnimatedSprite.flip_h = false;
		else:
			$AnimatedSprite.flip_h = true;
	
	if (is_on_floor() && !jumping):
		velocity.y = 0;
		
		if (Input.is_action_just_pressed("jump") && !GlobalVariables.player.dashed):
			$JumpDelay.start(0.2);
			$AnimatedSprite.frame = 0;
			$AnimatedSprite.play("jump");

	
	velocity.y += 3;
	
	if (Input.is_action_just_released("jump") && !is_on_floor() && velocity.y < 0):
		velocity.y *= 0.6;
	
	velocity.y = move_and_slide(velocity, Vector2(0, -1), true).y;
	jumping = false;


func _on_jumpDelay() -> void:
	jumping = true;
	velocity.y = -jump_height;
	var dust_particles = preload("res://src/particles/Dust.tscn").instance()
	dust_particles.position = self.position;
	$".".get_parent().add_child(dust_particles);
	
