extends StaticBody2D

const ITEM_TYPE = GlobalVariables.ItemTypes.GEM;

var collected := false setget set_collected;

func _physics_process(delta: float) -> void:
	if (!$AnimationPlayer.is_playing()):
		$AnimationPlayer.play("float");

func reset(player) -> void:
	if (player != null) :
		player.disconnect("hit_floor", self, "reset");
	self.collected = false;

func set_collected(value: bool) -> void:
	collected = value;
	if (value):
		$GemCollectParticles.restart();
	$CollisionShape2D.set_deferred("disabled", value);
	$Sprite.set_deferred("visible", !value);


func _on_Soul_activated() -> void:
	pass # Replace with function body.


func que_free() -> void:
	pass # Replace with function body.
