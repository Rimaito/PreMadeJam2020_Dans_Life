extends KinematicBody2D


export(Vector2) var velocity := Vector2(20, 0);


func _physics_process(delta: float) -> void:
	if (is_on_wall() || $GroundDetector.get_collider() == null):
		velocity.x *= -1;
		$GroundDetector.position.x *= -1;
	
	move_and_slide(velocity, Vector2(0, -1));
	update_animation();

func update_animation() -> void:
	if (velocity.x > 0):
		$AnimatedSprite.flip_h = false;
	else:
		$AnimatedSprite.flip_h = true;
