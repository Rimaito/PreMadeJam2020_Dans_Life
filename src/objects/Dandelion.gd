extends StaticBody2D

const ITEM_TYPE := GlobalVariables.ItemTypes.DANDELION;

export(String) var id :String;

onready var spawnpoint := position;
var collected := false setget set_collected;
var following := false;

func _ready() -> void:
	#Removes from scene if already collected
	if (GlobalVariables.collected_dandelions.has(id)):
		queue_free();

func _physics_process(delta: float) -> void:
	if (!$AnimationPlayer.is_playing()):
		$AnimationPlayer.play("float");
	
	if (following):
		position = to_local(to_global(position).linear_interpolate(GlobalVariables.player.position, 0.03));

func set_collected(value: bool) -> void:
	collected = value;
	$CollisionShape2D.set_deferred("disabled", true);
	following = value;

func reset(player: Player):
	self.collected = false;
	position = spawnpoint;
	$CollisionShape2D.set_deferred("disabled", false);
	player.disconnect("died", self, "reset");
	player.disconnect("hit_floor", self, "final_collected");

func final_collected() -> void:
	GlobalVariables.add_collected_dandelion(id);
	queue_free();
