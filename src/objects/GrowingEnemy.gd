extends KinematicBody2D

var solid := false;
const REGROW_TIME = 4;

export(Vector2) var velocity := Vector2(10, 0);

func _ready() -> void:
	pass

func _physics_process(delta: float) -> void:
	if (!solid):
		if (is_on_wall() || $GroundDetector.get_collider() == null):
			velocity.x *= -1;
			$GroundDetector.position.x *= -1;
		
		move_and_slide(velocity, Vector2(0, -1));
		update_animation();

func grow(_body: Node) -> void:
	solid = true;
	remove_from_group("enemy");
	$AnimatedSprite.play("shrink", true);
	$SolidHitBox.set_deferred("disabled", false);
	$ShrinkTimer.start(REGROW_TIME);
	$WalkTimer.stop();

func shrink() -> void:
	$AnimatedSprite.play("shrink");
	$SolidHitBox.set_deferred("disabled", true);
	$WalkTimer.start(1);
	
func walk() -> void:
	solid = false;
	add_to_group("enemy");
	$AnimatedSprite.play("right");

func update_animation() -> void:
	if (!solid) :
		if (velocity.x > 0):
			$AnimatedSprite.flip_h = false;
		else:
			$AnimatedSprite.flip_h = true;
