tool
extends StaticBody2D

const ParticleSpawner = preload("res://src/particles/ScentParticlesSpawner.tscn");

signal activated;
const ITEM_TYPE = GlobalVariables.ItemTypes.SOUL;

export(String) var id: String;

var collected := false;

func _get_configuration_warning() -> String:
	if (find_node("ScentPath") == null):
		return "Needs to have a child named ScentPath";
	return "";

func show_tray() -> void:
	if (!GlobalVariables.collected_souls.has(id)):
		emit_signal("activated");
		GlobalVariables.collected_souls.append(id);
		find_node("ScentPath").add_child(ParticleSpawner.instance());
