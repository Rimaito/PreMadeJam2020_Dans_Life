extends CanvasLayer
class_name UI

const TextBox := preload("res://src/ui/TextBox.tscn");

func _ready() -> void:
	GlobalVariables.connect("update_ui", self, "update_ui");
	GlobalVariables.ui = self;
	update_ui();

func show_text_box(text: String) -> void:
	var textbox := TextBox.instance();
	textbox.text = text;
	add_child(textbox);
	
func show_text_boxes(textes: Array, index: int = 0) -> void:
	if (index < textes.size()):
		var textBox := TextBox.instance();
		textBox.text = textes[index] as String;
		textBox.connect("on_closed", self, "show_text_boxes", [textes, index+1]);
		call_deferred("add_child", textBox);
		
func update_ui() -> void:
	$Dandelion/Label.text = "x" + GlobalVariables.collected_dandelions.size() as String;
