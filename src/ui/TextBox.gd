extends Node2D

signal on_closed;

export(String, MULTILINE) var text: String = "Lorep Ipsum";

func _ready() -> void:
	$AnimationPlayer/RichTextLabel.bbcode_text = text;
	$AnimationPlayer/RichTextLabel.update();
	$AnimationPlayer.play("text");
	get_tree().paused = true;

func _process(delta: float) -> void:
	if (Input.is_action_just_pressed("ui_accept")):
		if ($AnimationPlayer.is_playing()):
			$AnimationPlayer.playback_speed = 10;
		else:
			get_tree().paused = false;
			emit_signal("on_closed");
			queue_free();
