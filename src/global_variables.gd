tool
extends Node

signal update_ui;

var player: Player;
var ui: UI;

var collected_dandelions := [];
var collected_souls :=[];
enum ItemTypes {GEM, DANDELION, SPAWNPOINT, SOUL}

func add_collected_dandelion(id: String) -> void:
	collected_dandelions.append(id);
	emit_signal("update_ui");
