extends CPUParticles2D

func _ready() -> void:
	restart();
	$Delete.start(5);


func _on_Delete_timeout() -> void:
	queue_free();
