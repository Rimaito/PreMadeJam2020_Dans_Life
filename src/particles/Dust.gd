extends Node2D
	
func _ready() -> void:
	restart();

func restart() -> void:
	$left.restart();
	$right.restart();
	$Delete.start(1.5);

func _on_Delete_timeout() -> void:
	queue_free();
