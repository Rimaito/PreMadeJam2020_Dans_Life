extends CPUParticles2D

export(bool) var part_of_track := false;
export(float) var intervall := 3.0;


func _ready() -> void:
	if (part_of_track):
		one_shot = true;
		lifetime = intervall / 4;
	else:
		one_shot = false;
	
	$Timer.start(intervall);


func _on_Timer_timeout() -> void:
	restart();
	$Timer.start(intervall);
