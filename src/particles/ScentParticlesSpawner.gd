extends PathFollow2D

export(float) var intervall := 3;
export(Color) var particle_color := Color("#a400ff");
const DISTANCE := 16;

var ScentParticle := preload("res://src/particles/Scent.tscn");

var lastSet := 0.0;

func _physics_process(_delta: float) -> void:
	var prev_offset := offset;
	offset += 1.3;
	
	if (prev_offset == offset):
		queue_free();
	
	if (lastSet + DISTANCE < offset):
		lastSet = offset;
		var particles = ScentParticle.instance();
		particles.part_of_track = true;
		particles.intervall = intervall;
		particles.color = particle_color;
		particles.position = position;
		get_parent().add_child(particles);
		
