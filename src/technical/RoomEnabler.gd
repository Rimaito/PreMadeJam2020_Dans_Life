extends VisibilityNotifier2D
class_name RoomEnabler

func _ready() -> void:
	#disable(get_parent());
	pass

func disable(node: Node) -> void:
	if (!node.is_in_group("keep_alive")):
		node.set_physics_process(false);
		node.set_process(false);
		if (node is CPUParticles2D):
			node.emitting = false;
		elif (node is AnimatedSprite):
			node.playing = false;
		elif (node is AnimationPlayer):
			node.stop(false);
		elif (node is CollisionShape2D):
			node.set_deferred("disabled", true);
		
		for child in node.get_children():
			disable(child);

func enable(node: Node) -> void:
	if (!node.is_in_group("keep_alive")):
		node.set_physics_process(true);
		node.set_process(true);
		if (node is CPUParticles2D):
			node.emitting = true;
		if (node is AnimatedSprite):
			node.playing = true;
		if (node is AnimationPlayer):
			node.play();
		elif (node is CollisionShape2D):
			node.set_deferred("disabled", false);
		
		
		for child in node.get_children():
			enable(child);


func _on_RoomEnabler_viewport_entered(viewport: Viewport) -> void:
	enable(get_parent());


func _on_RoomEnabler_viewport_exited(viewport: Viewport) -> void:
	disable(get_parent());
