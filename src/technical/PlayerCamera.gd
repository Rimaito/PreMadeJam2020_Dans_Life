extends Camera2D

const DEFAULT_LIMIT = 10000000;

func _physics_process(delta: float) -> void:
	$Position2D.position = to_local(get_camera_screen_center());
	
	var top := true;
	var bottom := true;
	var left := true;
	var right := true;
	
	for node in $Position2D/top.get_children():
		if (node.get_overlapping_bodies() as Array).size() == 0:
			top = false;
			break;
	
	for node in $Position2D/left.get_children():
		if (node.get_overlapping_bodies() as Array).size() == 0:
			left = false;
			break;
	
	for node in $Position2D/bottom.get_children():
		if (node.get_overlapping_bodies() as Array).size() == 0:
			bottom = false;
			break;
	
	for node in $Position2D/right.get_children():
		if (node.get_overlapping_bodies() as Array).size() == 0:
			right = false;
			break;
	
	if (get_camera_screen_center().distance_to(to_global(position)) > 200):
		top = false;
		bottom = false;
		left = false;
		right = false
	
	if (top):
		if (limit_top == -DEFAULT_LIMIT):
			limit_top = $Position2D/top.global_position.y;
	else:
		limit_top = -DEFAULT_LIMIT;
	
	if (left):
		if (limit_left == -DEFAULT_LIMIT):
			limit_left = $Position2D/left.global_position.x;
	else:
		limit_left = -DEFAULT_LIMIT;
	
	if (bottom):
		if (limit_bottom == DEFAULT_LIMIT):
			limit_bottom = $Position2D/bottom.global_position.y;
	else:
		limit_bottom = DEFAULT_LIMIT;
	
	if (right):
		if (limit_right == DEFAULT_LIMIT):
			limit_right = $Position2D/right.global_position.x;
	else:
		limit_right = DEFAULT_LIMIT;
